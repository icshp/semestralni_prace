﻿using System;
using System.Configuration;
using cisco.Core.Properties;

namespace cisco.Core.Helper
{
    /// <summary>
    /// Třída sloužící pro ukládání nastavení Telnetu
    /// </summary>
    public static class TelnetSettings
    {
        public static int Port
        {
            get { return (int)Settings.Default.TelnetPort; }
            set { Settings.Default.TelnetPort = value; }
        }

        /// <summary>
        /// Nastavení továrních hodnot
        /// </summary>
        public static void SetDefaultSettings()
        {
            Port = Settings.Default.TelnetDefaultPort;
            SaveSettings.Save();
        }
    }

}

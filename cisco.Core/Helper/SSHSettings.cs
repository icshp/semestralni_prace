﻿using System;
using System.Configuration;
using cisco.Core.Properties;

namespace cisco.Core.Helper
{
    /// <summary>
    /// Třída sloužící pro obsluhu ukládání nastavení SSH
    /// </summary>
    public static class SSHSettings
    {
        public static int Port
        {
            get { return (int)Settings.Default.SSHPort; }
            set { Settings.Default.SSHPort = value; }
        }

        /// <summary>
        /// Načtení výchozího nastavení
        /// </summary>
        public static void SetDefaultSettings()
        {
            Port = Settings.Default.SSHDefaultPort;
            SaveSettings.Save();
        }
    }
}

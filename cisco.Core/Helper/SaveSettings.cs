﻿using cisco.Core.Properties;
using System.Configuration;

namespace cisco.Core.Helper
{
    public static class SaveSettings
    {
        /// <summary>
        /// Uložení hodnot do paměti
        /// </summary>
        public static void Save()
        {
            Settings.Default.Save();
        }
    }
}

﻿using cisco.Core.Properties;
using System;
using System.Configuration;
using cisco.Core.Services;

namespace cisco.Core.Helper
{
    public static class TFTPSettings
    {
        public static string Directory
        {
            get
            {
                if (Settings.Default.TftpDirectory == "")
                {
                    SetDefaultSettings();
                    return Directory;
                }
                else
                {
                    return (string)Settings.Default.TftpDirectory;
                }
            }
            set { Settings.Default.TftpDirectory = value; }
        }

        /// <summary>
        /// Nastavení továrních hodnot
        /// </summary>
        public static void SetDefaultSettings()
        {
            Directory = Environment.CurrentDirectory;
            SaveSettings.Save();
        }
    }
}

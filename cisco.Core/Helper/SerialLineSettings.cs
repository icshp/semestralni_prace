﻿using System;
using System.Configuration;
using cisco.Core.Properties;
using System.IO.Ports;

namespace cisco.Core.Helper
{
    /// <summary>
    /// Třída obsluhující ukládání nastavení Seriové linky.
    /// Data jsou ukládána do Properties->Settings.Settings
    /// </summary>
    public static class SerialLineSettings
    {
        public static int Speed
        {
            get { return (int)Settings.Default.SerialLineSpeed; }
            set { Settings.Default.SerialLineSpeed = value; }
        }

        public static int DataBids
        {
            get { return (int)Settings.Default.SerialLineDataBids; }
            set { Settings.Default.SerialLineDataBids = value; }
        }

        public static Parity Parity
        {
            get { return Settings.Default.SerialLineParity; }
            set { Settings.Default.SerialLineParity = value; }
        }

        public static StopBits StopBits
        {
            get { return Settings.Default.SerialLineStopBits; }
            set { Settings.Default.SerialLineStopBits = value; }
        }

        public static Handshake Handshake
        {
            get { return Settings.Default.SerialLineHandshake; }
            set { Settings.Default.SerialLineHandshake = value; }
        }

        /// <summary>
        /// Nastavení všech hodnot do továrního nastavení
        /// </summary>
        public static void SetDefaultSettings()
        {
            Speed = Settings.Default.SerialLineDefaultSpeed;
            DataBids = Settings.Default.SerialLineDefaultDataBids;
            Parity = Settings.Default.SerialLineDefaultParity;
            StopBits = Settings.Default.SerialLineDefautlStopBits;
            Handshake = Settings.Default.SerialLineDefaultHandshake;
            SaveSettings.Save();
        }
    }
}

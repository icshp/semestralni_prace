﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Windows.Input;

namespace cisco.Core.Services
{
    public class SerialLineService : IConnection
    {

        public EventHandler DataReceived;

        private SerialPort _serialPort;

        /// <summary>
        /// Vrátí všechny dostupné seriové linky
        /// </summary>
        /// <returns>
        /// Dostupné seriové linky v List<string>
        /// </returns>
        public static List<string> GetSerialLines()
        {
            List<string> SerialLines = new List<string>();
            foreach (string s in SerialPort.GetPortNames())
            {
                SerialLines.Add(s);
            }
            return SerialLines;
        }

        /// <summary>
        /// Metoda pro vytvoření spojení k seriové lince
        /// </summary>
        /// <example>
        /// <code>
        /// SerialLineService sl = new SerialLineService();
        /// sl.Connect("COM1");
        /// </code>
        /// </example>
        public void Connect(string device)
        {
            try
            {
                if (_serialPort != null)
                {
                    if (_serialPort.IsOpen)
                    {
                        _serialPort.Close();
                    }
                }

                _serialPort = new SerialPort(device, Helper.SerialLineSettings.Speed, Helper.SerialLineSettings.Parity, Helper.SerialLineSettings.DataBids, Helper.SerialLineSettings.StopBits);
                _serialPort.Handshake = Helper.SerialLineSettings.Handshake;
                _serialPort.ReadTimeout = 500;
                _serialPort.WriteTimeout = 500;
                _serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                _serialPort.Open();
                _serialPort.Write(new byte[] { 13, 10 }, 0, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metoda pro oblsluhu příchozích dat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(500);
            string dataReceived = _serialPort.ReadExisting();
            UpdateEvent(dataReceived);
        }

        /// <summary>
        /// Metoda pro odeslání zprávy přes sériovou liku
        /// </summary>
        public void WriteLine(string message)
        {
            try
            {
                _serialPort.WriteLine(message);
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metoda pro odeslání znaku přes sériovou liku
        /// </summary>
        public void Write(string character)
        {
            try
            {
                _serialPort.Write(character);
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metoda pro ukončení spojení se sériovou linkou
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (_serialPort != null)
                {
                    _serialPort.Close();
                    _serialPort = null;
                }
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Pomocná metoda pro volání EventHandleru
        /// </summary>
        private void UpdateEvent(string msg)
        {
            if (DataReceived != null)
                DataReceived(msg, EventArgs.Empty);
        }
    }
}

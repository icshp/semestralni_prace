﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Renci.SshNet;

namespace cisco.Core.Services
{
    public class SSHService
    {
        private SshClient _sshClient;
        private ShellStream _shellStream;
        private StreamReader _reader;
        private StreamWriter _writer;
        private bool _isListening;
        private Thread _readThread;

        public event EventHandler DataReceived;

        public SSHService()
        {
            _isListening = false;
        }

        /// <summary>
        /// Metoda pro připojení pomocí SSH
        /// </summary>
        /// <param name="address"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void Connect(string address, string username, string password)
        {
            Debug.WriteLine(address + " " + username + " " + password);
            try
            {
                _sshClient = new SshClient(address, 22, username, password);
                _sshClient.Connect();
                _sshClient.ErrorOccurred += _sshClient_ErrorOccurred;
                _shellStream = _sshClient.CreateShellStream("Terminal", 80, 60, 800, 600, 65536);
                _reader = new StreamReader(_shellStream, Encoding.UTF8, true, 1024, true);
                _writer = new StreamWriter(_shellStream) { AutoFlush = true };
                _isListening = true;
                _readThread = new Thread(ReceiveData) { IsBackground = true };
                _readThread.Start();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metoda pro ukončení spojení
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (_readThread != null)
                {
                    _readThread.Abort();
                    _sshClient.Disconnect();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Error handler 
        /// (Jen pro vývoj)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _sshClient_ErrorOccurred(object sender, Renci.SshNet.Common.ExceptionEventArgs e)
        {
            Debug.WriteLine("chyba" + e.ToString());
        }

        /// <summary>
        /// Odposlech příchozích dat
        /// </summary>
        private void ReceiveData()
        {
            while (true)
            {
                try
                {
                    if (_reader != null)
                    {
                        Thread.Sleep(500);
                        StringBuilder result = new StringBuilder();

                        string line;
                        while ((line = _reader.ReadLine()) != null)
                        {
                            result.AppendLine(line);
                        }

                        if (!string.IsNullOrEmpty(result.ToString()))
                        {
                            UpdateEvent(result.ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                }
            }
        }


        /// <summary>
        /// Metoda pro odeslání dat
        /// </summary>
        /// <param name="message"></param>
        public void WriteLine(string message)
        {
            _writer.Write(message+"\n");
        }

        /// <summary>
        /// Metoda pro odeslání dat
        /// </summary>
        /// <param name="message"></param>
        public void Write(string message)
        {
            _writer.Write(message);
        }

        /// <summary>
        /// Metoda pro obsluhu eventu
        /// </summary>
        private void UpdateEvent(string message)
        {
            if (DataReceived != null)
                DataReceived(message, EventArgs.Empty);
        }

    }
}

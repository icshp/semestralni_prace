﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using cisco.Core.Helper;

namespace cisco.Core.Services
{
    /// <summary>
    /// Třída pro obsluhu protokolu Telnet
    /// </summary>
    public class TelnetService : IConnection
    {
        public event EventHandler DataReceived;
        private Thread _readThread;
        public bool Exiting;
        private NetworkStream _networkStream;
        private TcpClient _clientSocket;

        public TelnetService()
        {
            _networkStream = default(NetworkStream);
            _clientSocket = new TcpClient();
            Exiting = false;
        }


        /// <summary>
        /// Metoda pro připojení k telnetu
        /// </summary>
        public void Connect(string ip)
        {
            try
            {
                _clientSocket.ReceiveTimeout = 500;
                _clientSocket.Connect(ip, 23);
                _readThread = new Thread(ReceiveData){ IsBackground = true };
                _readThread.Start();
            }
            catch (Exception ex)
            {
                throw ex;     
            }
        }

        /// <summary>
        /// Metoda pro odpojení od telnetu
        /// </summary>
        public void Disconnect()
        {
            try
            {
                if (_readThread != null)
                {
                    Exiting = true;
                    _networkStream.Close();
                    _clientSocket.Dispose();

                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Metoda pro odeslání zprávy přes telnet (po znacích)
        /// </summary>
        public void Write(string message)
        {
            byte[] outstream = Encoding.ASCII.GetBytes(message);

            _networkStream.Write(outstream, 0, outstream.Length);
            _networkStream.Flush();
        }

        /// <summary>
        /// Metoda pro odeslání zprávy přes telnet (po řádkách)
        /// </summary>
        public void WriteLine(string message)
        {
            Write(message + "\n");
        }

        /// <summary>
        /// Metoda pro obsluhu příchozích dat a následné volání pomocné metody.
        /// </summary>
        private void ReceiveData()
        {
            _networkStream = _clientSocket.GetStream();
            var buffsize = _clientSocket.ReceiveBufferSize;
            byte[] inStream = new byte[buffsize];
            while (true)
            {
                if (Exiting)
                {
                    _readThread.Abort();
                }
                if (_networkStream.DataAvailable)
                {
                    Thread.Sleep(500);
                    inStream = new byte[buffsize];
                    int numberOfBytesRead = _networkStream.Read(inStream, 0, inStream.Length);
                    StringBuilder message = new StringBuilder();
                    message.AppendFormat("{0}", Encoding.ASCII.GetString(inStream, 0, numberOfBytesRead));
                    UpdateEvent(message.ToString().Trim());
                }
            }
        }

        /// <summary>
        /// Metoda pro obsluhu eventu
        /// </summary>
        private void UpdateEvent(string msg)
        {
            if (DataReceived != null)
                DataReceived(msg, EventArgs.Empty);
        }
    }
}

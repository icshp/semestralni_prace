﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Core.Services
{
    interface IConnection
    {
        void Connect(string device);

        void Disconnect();

        void WriteLine(string message);

        void Write(string message);

    }
}

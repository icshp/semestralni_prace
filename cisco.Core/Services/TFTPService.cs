﻿using cisco.Core.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Tftp.Net;

namespace cisco.Core.Services
{
    /// <summary>
    /// Třída sloužící pro obsluhu TFTP serveru. 
    /// Implementováno přesně dle návodu k knihovně Tftp.Net;
    /// @SEE https://github.com/Callisto82/tftp.net
    /// </summary>
    public static class TFTPService
    {
        public static EventHandler Report;
        public static bool IsConnected { get; private set; }
        private static TftpServer _server;

        /// <summary>
        /// Konstruktor
        /// </summary>
        static TFTPService()
        {
            IsConnected = false;
        }

        /// <summary>
        /// Metoda pro spuštění TFTP serveru
        /// </summary>
        public static void StartServer()
        {
            try
            {
                _server = new TftpServer();
                _server.OnReadRequest += new TftpServerEventHandler(OnReadRequest);
                _server.OnWriteRequest += new TftpServerEventHandler(OnWriteRequest);
                _server.Start();
                IsConnected = true;
                UpdateEvent("TFTP server je aktivní");
            }
            catch (Exception e)
            {
                IsConnected = false;
                UpdateEvent("Spuštění serveru se nezdařilo! " + e);
            }        
        }

        /// <summary>
        /// Metoda pro vypnutí TFTP serveru 
        /// </summary>
        public static void StopServer()
        {
            if (IsConnected)
            {
                _server.Dispose();
                IsConnected = false;
                UpdateEvent("Server TFTP je neaktivní!");
            }
        }

        /// <summary>
        /// Metoda obsluhující příchozí požadavek na zápis
        /// </summary>
        static void OnWriteRequest(ITftpTransfer transfer, EndPoint client)
        {
            try
            {
                String file = Path.Combine(TFTPSettings.Directory, transfer.Filename);

                if (!File.Exists(file))
                {
                    OutputTransferStatus(transfer, "Příchozí soubor od: " + client);
                    StartTransfer(transfer, new FileStream(file, FileMode.CreateNew));
                }
                else
                {
                    CancelTransfer(transfer, TftpErrorPacket.FileAlreadyExists);
                }
            }
            catch (Exception)
            {
                UpdateEvent("Místo pro uložení není dostupné. Ověřte prosím konfiguraci TFTP!");
            }
        }

        /// <summary>
        /// Metoda obsluhující příchozí požadavek na čtení
        /// </summary>
        static void OnReadRequest(ITftpTransfer transfer, EndPoint client)
        {
            String path = Path.Combine(TFTPSettings.Directory, transfer.Filename);
            FileInfo file = new FileInfo(path);

            //Is the file within the server directory?
            if (!file.FullName.StartsWith(TFTPSettings.Directory, StringComparison.InvariantCultureIgnoreCase))
            {
                CancelTransfer(transfer, TftpErrorPacket.AccessViolation);
            }
            else if (!file.Exists)
            {
                CancelTransfer(transfer, TftpErrorPacket.FileNotFound);
            }
            else
            {
                OutputTransferStatus(transfer, "Přijat požadavek na čtení od: " + client);
                StartTransfer(transfer, new FileStream(file.FullName, FileMode.Open));
            }
        }

        /// <summary>
        /// Metoda obsluhující událost "Přenos dat na server"
        /// </summary>
        private static void StartTransfer(ITftpTransfer transfer, Stream stream)
        {
            transfer.OnProgress += new TftpProgressHandler(OnProgress);
            transfer.OnError += new TftpErrorHandler(OnError);
            transfer.OnFinished += new TftpEventHandler(OnFinished);
            transfer.Start(stream);
        }

        /// <summary>
        /// Metoda obsluhující událost "Zrušená transakce"
        /// </summary>
        private static void CancelTransfer(ITftpTransfer transfer, TftpErrorPacket reason)
        {
            OutputTransferStatus(transfer, "byl zrušen." + reason.ErrorMessage);
            transfer.Cancel(reason);
        }

        /// <summary>
        /// Metoda obsluhující událost "Chyba"
        /// </summary>
        static void OnError(ITftpTransfer transfer, TftpTransferError error)
        {
            OutputTransferStatus(transfer, "vyvolal chybu: " + error);
        }

        /// <summary>
        /// Metoda obsluhující událost "Dokončeno"
        /// </summary>
        static void OnFinished(ITftpTransfer transfer)
        {
            OutputTransferStatus(transfer, "byl dokončen.");
        }

        /// <summary>
        /// Metoda obsluhující událost "Průběh přenosu"
        /// </summary>
        static void OnProgress(ITftpTransfer transfer, TftpTransferProgress progress)
        {
            OutputTransferStatus(transfer, "je v průběhu " + progress);
        }

        /// <summary>
        /// Metoda vyvolávající událost, která slouží pro UI
        /// </summary>
        private static void OutputTransferStatus(ITftpTransfer transfer, string message)
        {
            string msg = "Přenos souboru: " + transfer.Filename + " " + message;
            UpdateEvent(msg);
        }

        /// <summary>
        /// Pomocná metoda pro vyvolání eventu
        /// </summary>
        private static void UpdateEvent(string msg)
        {
            if (Report != null)
                Report(msg, EventArgs.Empty);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Core.Models
{
    public enum CredentialType
    {
        ENABLED,
        CONFIGURE,
        SSH,
        SERIAL
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Core.Models
{
    public class Credential
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public CredentialType Type { get; set; }
    }
}

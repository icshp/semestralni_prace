﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Core.Models
{
    /// <summary>
    /// Objekt Device
    /// Slouží pro ukládání parametrů zařízení
    /// </summary>
    public class Device
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DeviceTypeEnum TypeOfDevice { get; set; } 

        public string IP { get; set; }

        public string SerialPort { get; set; }

        public bool TelnetEnabled { get; set; }

        public bool SSHEnabled { get; set; }

        //Objekt
        public Object Label;

        //Spojení s jinými objekty
        public List<object> Connections;

        //Přihlašovací údaje k SSH, Exec, Conf. mode
        public List<Credential> Credentials;

        /// <summary>
        /// Konstruktor pro prázdný objekt
        /// </summary>
        public Device()
        {
            Credentials = new List<Credential>();
            Connections = new List<object>();
        }

        /// <summary>
        /// Konstruktor pro tvorbu objektu
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="deviceTypeEnum"></param>
        public Device(string name, string description, DeviceTypeEnum deviceTypeEnum) : this()
        {
            Name = name;
            Description = description;
            TypeOfDevice = deviceTypeEnum;
        }

        /// <summary>
        /// Metoda převodu objektu do string
        /// </summary>
        /// <returns>název zařízení</returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}

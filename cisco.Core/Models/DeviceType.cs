﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Core.Models
{
    /// <summary>
    /// Model - Device Type
    /// </summary>
    public enum DeviceTypeEnum
    {
        SWITCH,
        ROUTER,
        PC
    }

    /// <summary>
    /// Třída obsluhující DeviceType
    /// </summary>
    public class DeviceType {

        /// <summary>
        /// Metoda, která vrátí list všech dostupných typů zařízení.
        /// </summary>
        /// <returns>
        /// List obsahující zařízení
        /// </returns>
        public static List<DeviceTypeEnum> GetTypes()
        {
            List<DeviceTypeEnum> list = new List<DeviceTypeEnum>();
            list.Add(DeviceTypeEnum.SWITCH);
            list.Add(DeviceTypeEnum.ROUTER);
            return list;
        }

        /// <summary>
        /// Vrátí cestu k ikoně
        /// </summary>
        /// <returns>
        /// string cesta
        /// </returns>
        public static string GetIcon(DeviceTypeEnum device)
        {
            switch (device)
            {
                case DeviceTypeEnum.SWITCH:
                    return @"\Files\Icons\switch.png";
                case DeviceTypeEnum.ROUTER:
                    return @"\Files\Icons\router.png";
                case DeviceTypeEnum.PC:
                    return @"\Files\Icons\pc.png";
                default:
                    throw new Exception("Nespecifikovaný typ zařízení.");
            }
        }

        /// <summary>
        /// Metoda sloužící pro převod ze string na Enum.TypeOfDevice
        /// Slouží pro kontrolu, zda dané zařízení existuje.
        /// </summary>
        /// <returns>
        /// TypeOfDevice enum
        /// </returns>
        public static DeviceTypeEnum GetDeviceTypeEnum(string data) {
            switch (data)
            {
                case "SWITCH":
                    return DeviceTypeEnum.SWITCH;
                case "ROUTER":
                    return DeviceTypeEnum.ROUTER;
                case "PC":
                    return DeviceTypeEnum.PC;
                default:
                    throw new Exception("Nespecifikovaný typ zařízení.");
            }
        }

    }
}

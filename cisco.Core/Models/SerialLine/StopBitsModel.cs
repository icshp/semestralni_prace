﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace cisco.Core.Models.SerialLine
{
    public static class StopBitsModel
    {
        public static List<StopBits> List { get; }

        static StopBitsModel()
        {
            List = new List<StopBits> { StopBits.None, StopBits.One, StopBits.Two, StopBits.OnePointFive };
        }
    }
}

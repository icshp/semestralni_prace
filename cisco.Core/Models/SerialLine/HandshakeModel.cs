﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace cisco.Core.Models.SerialLine
{
    public static class HandshakeModel
    {
        public static List<Handshake> List { get; }

        static HandshakeModel()
        {
            List = new List<Handshake> { Handshake.None, Handshake.XOnXOff, Handshake.RequestToSend, Handshake.RequestToSendXOnXOff };
        }
    }
}

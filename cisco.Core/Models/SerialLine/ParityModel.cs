﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace cisco.Core.Models.SerialLine
{
    public static class ParityModel
    {
        public static List<Parity> List { get; }

        static ParityModel()
        {
            List = new List<Parity> { Parity.None, Parity.Even, Parity.Mark, Parity.Odd, Parity.Space };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Core.Models.SerialLine
{
    public static class SpeedModel
    {
        public static List<int> List { get; }

        static SpeedModel()
        {
            List = new List<int> { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };
        }
    }
}

Téma: 
> Aplikace pro konfiguraci Cisco zařízení

Základní body:
	1) Připojení přes Seriovou linku/USB-To-Serial (Implementováno).
	2) Připojení přes Telnet (Implementováno).
	3) Připojení přes SSH (Implementováno).
	--> Připojení musí umožňovat Navázat komunikaci, přijmout, odeslat nějaký řetězec textu. <--
	4) TFTP server na který se může stáhnout konfigurace z Cisco zařízení a naopak (Implementováno).
	5) Správce zařízení - správa typu zařízení, připojení a možnost popisu (Implementováno).
	6) O aplikaci (Implementováno).
	7) Konzole (CLI) pro možnost komunikace (Implementováno).

Rozširůjící body:
	1) Knihovna příkazů pro Cisco (Implementováno).
	2) Na hlavní straně bude možné tvořit "spojení" (Implementováno).

Popis ovládání:
Main (canvas):
	1) Pravé tlačítko myši -> Vytvořit nové zařízení
Main (image/zařízení):
	1) Levé tlačítko myši -> Konfigurace zařízení
	2) Pravé tlačítko myši -> Smazat zařízení
	3) Kolečko myši -> Přesun objektu/zařízení.
	4) Ctrl + Levé tlačítko -> Tvorba spojení (nutné mít dva objekty!)
Položky menu v Main:
Nastavení: 
	Konfigurace služeb (TFTP, Telnet, SSH, Serial line)
	O aplikaci
Služby:
	Spuštění/Vypnutí TFTP serveru
Ostatní okna:
Konfigurace zařízení:
	Slouží pro správu zařízení v aplikaci.
		-> Nastavení jména
		-> Nastavení popisu
		-> Typ + IP -> Možnost spojení
Konfigurační nástroj:
	CLI -> Klasická konzole
	Knihovna příkazů -> Příklady příkazů





﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Models
{
    class Connection
    {
        public object Line;
        public bool First;
        public object Target;

        public Connection(bool first, object line, object target) {
            Target = target;
            Line = line;
            First = first;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cisco.Models
{
    public class Command
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Commands { get; set; }

        public override string ToString()
        {
            return Name.ToString();
        }
    }
}

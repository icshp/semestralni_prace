﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using cisco.Core.Models.SerialLine;
using System.IO.Ports;
using cisco.Core.Helper;

namespace cisco.Views.Settings
{
    /// <summary>
    /// Logická část okna SerialLineConfig.xaml
    /// </summary>
    public partial class SerialLineConfig : Window
    {
        public SerialLineConfig()
        {
            InitializeComponent();
            LoadSettings();
        }

        private void LoadSettings() {
            //Speed
            cbSpeed.ItemsSource = SpeedModel.List;
            cbSpeed.Text = SerialLineSettings.Speed.ToString();
            //DataBids
            txtDataBids.Text = SerialLineSettings.DataBids.ToString();
            //StopBids
            cbStopBits.ItemsSource = StopBitsModel.List;
            cbStopBits.SelectedValue = SerialLineSettings.StopBits;
            //Parity
            cbParity.ItemsSource = ParityModel.List;
            cbParity.SelectedValue = SerialLineSettings.Parity;
            //Handshake
            cbHandshake.ItemsSource = HandshakeModel.List;
            cbHandshake.SelectedValue = SerialLineSettings.Handshake;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SerialLineSettings.Speed = Convert.ToInt32(cbSpeed.Text);
                SerialLineSettings.DataBids = Convert.ToInt32(txtDataBids.Text);
                SerialLineSettings.StopBits = (StopBits) cbStopBits.SelectedValue;
                SerialLineSettings.Parity = (Parity) cbParity.SelectedValue;
                SerialLineSettings.Handshake = (Handshake) cbHandshake.SelectedValue;
                SaveSettings.Save();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDefaults_Click(object sender, RoutedEventArgs e)
        {
            SerialLineSettings.SetDefaultSettings();
            LoadSettings();
            MessageBox.Show("Výchozí nastavení sériové linky bylo obnoveno.", "Informace", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}

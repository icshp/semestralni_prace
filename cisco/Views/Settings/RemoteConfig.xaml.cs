﻿using System;
using System.Windows;
using System.Windows.Forms;
using cisco.Core.Helper;
using cisco.Core.Services;

namespace cisco.Views.Settings
{
    /// <summary>
    /// Interaction logic for RemoteConfiguration.xaml
    /// </summary>
    public partial class RemoteConfiguration : Window
    {
        public RemoteConfiguration()
        {
            InitializeComponent();
            LoadSettings();
        }

        private void LoadSettings()
        {
            //Telnet
            txtTelnet.Text = TelnetSettings.Port.ToString();
            //SSH
            txtSSH.Text = SSHSettings.Port.ToString();
            //TFTP
            txtTftpDir.Text = TFTPSettings.Directory.ToString();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TelnetSettings.Port = Convert.ToInt32(txtTelnet.Text);
                SSHSettings.Port = Convert.ToInt32(txtSSH.Text);
                SaveSettings.Save();
                this.Close();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.ToString(), "Chyba", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnDefaults_Click(object sender, RoutedEventArgs e)
        {
            TelnetSettings.SetDefaultSettings();
            SSHSettings.SetDefaultSettings();
            TFTPSettings.SetDefaultSettings();
            LoadSettings();
            System.Windows.MessageBox.Show("Výchozí nastavení sériové linky bylo obnoveno.", "Informace", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void btnTftpDir_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "Zvolte prosím složku";
            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                TFTPSettings.Directory = dialog.SelectedPath;
            }
            LoadSettings();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace cisco.Views.Pages.DeviceConfigurator.Shared
{
    /// <summary>
    /// Logika pro BasicConfiguration.xaml
    /// </summary>
    public partial class BasicConfigurationPage : Page
    {
        public BasicConfigurationPage()
        {
            InitializeComponent();
        }

        #region [iOS] Uložení a mazání konfigurace z NVRAM
        private void btnDeleteConfiguration_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSaveConfiguration_Click(object sender, RoutedEventArgs e)
        {

        }

#endregion
        #region [TFTP] Záloha a smazání konfigurace

        private void btnDownloadConfiguration_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnUploadConfiguration_Click(object sender, RoutedEventArgs e)
        {

        }

        #endregion
    }
}

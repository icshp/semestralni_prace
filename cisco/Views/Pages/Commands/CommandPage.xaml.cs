﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using cisco.Models;

namespace cisco.Views.Pages.Commands
{
    /// <summary>
    /// Interaction logic for CommandPage.xaml
    /// </summary>
    public partial class CommandPage : Page
    {
        public CommandPage(Command command)
        {
            InitializeComponent();
            LbName.Content = command.Name;
            TbDescription.Text = command.Description;
            LvSupportedCommands.ItemsSource = command.Commands;
        }
    }
}

﻿using cisco.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace cisco.Views
{
    public partial class AddDevice : Window
    {
        public Device Result
        {
            get; private set;
        }

        public AddDevice()
        {
            InitializeComponent();
            cbDeviceType.ItemsSource = DeviceType.GetTypes();
            cbDeviceType.SelectedIndex = 0;
        }

        /// <summary>
        /// Vytvoření zařízení
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tbName.Text == "" ||  cbDeviceType.SelectedItem == null || tbName.Text.Length > 9)
            {
                MessageBox.Show("Není vyplněno potřebné pole, nebo je název příliš dlouhý!");
            }
            else
            {
                Result = new Device(tbName.Text, tbDescription.Text, (DeviceTypeEnum) cbDeviceType.SelectedItem);
                this.DialogResult = true;
                this.Close();
            }
        }
    }
}

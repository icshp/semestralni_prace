﻿using System;

using System.Windows;

using System.Windows.Input;

using cisco.Views.Settings;

using cisco.Core.Models;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Collections.Generic;
using cisco.Core.Services;
using System.Windows.Shapes;
using cisco.Models;

namespace cisco.Views
{

    /// <summary>
    /// Logika pro hlavní okno
    /// </summary>
    public partial class MainWindow : Window
    {
        private Dictionary<Image, Device> _deviceList;
        private Image _selectedImage, _selectedPoint;
        private double _width, _height;
        private bool drag, _line;
        public MainWindow()
        {
            InitializeComponent();
            _deviceList = new Dictionary<Image, Device>();
            canvas.MouseMove += Canvas_MouseMove;
            canvas.MouseUp += Canvas_MouseUp;
            canvas.SizeChanged += Canvas_SizeChanged;
            _selectedPoint = null;
            drag = false;
            _line = false;
        }

        /// <summary>
        /// Obsluhuje událost "změna velikosti" canvasu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            _width = canvas.ActualWidth;
            _height = canvas.ActualHeight;
        }

        /// <summary>
        /// Deaktivace přesunu objektu v Canvasu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            _selectedImage = null;
            drag = false;
        }

        /// <summary>
        /// Metoda obsluhující přesun objektu v Canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Canvas_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (drag)
                {
                    var newX = e.GetPosition(canvas).X - 20;
                    var newY = e.GetPosition(canvas).Y - 20;

                    Debug.WriteLine(String.Format("X:{0} Y:{1}", _width, newX));

                    if ((newX + _selectedImage.Width > _width) || (newY + _selectedImage.Height + 15 > _height) || newX < 0 || newY < 0)
                    {
                        return;
                    }
                    _selectedImage.SetValue(Canvas.TopProperty, newY);
                    _selectedImage.SetValue(Canvas.LeftProperty, newX);
                    Label lbl = (Label)_deviceList[_selectedImage].Label;
                    lbl.SetValue(Canvas.TopProperty, newY + 35);
                    lbl.SetValue(Canvas.LeftProperty, newX);
                    Device selectedDevice = _deviceList[_selectedImage];
                    foreach (Connection connection in selectedDevice.Connections)
                    {
                        connection.Line = MoveLine((Line)connection.Line, connection.First, newX + (_selectedImage.Width / 2), newY + (_selectedImage.Height / 2));
                        Device dev = _deviceList[(Image)connection.Target];
                        Debug.WriteLine(connection.Target);
                        foreach (Connection targetConnection in dev.Connections)
                        {
                            if (targetConnection.Target == selectedDevice)
                            {
                                targetConnection.Line = connection.Line;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        /// <summary>
        /// Konfigurace seriové linky
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuSettingsSerialLine_Click(object sender, RoutedEventArgs e)
        {
            SerialLineConfig slc = new SerialLineConfig();
            slc.Show();
        }

        /// <summary>
        /// Otevření nastavení Telnetu, SSH a TFTP serveru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuSettingsTelnetSSH_Click(object sender, RoutedEventArgs e)
        {
            RemoteConfiguration rc = new RemoteConfiguration();
            rc.Show();
        }

        /// <summary>
        /// Otevření "O aplikaci"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuSettingsAboutApplication_Click(object sender, RoutedEventArgs e)
        {
            AboutApplication ap = new AboutApplication();
            ap.Show();
        }

        /// <summary>
        /// Tvorba zařízení v Canvasu 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void canvas_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            Image img = new Image();

            Point p = Mouse.GetPosition(canvas);
            Canvas.SetLeft(img, p.X);
            Canvas.SetTop(img, p.Y);

            img.Stretch = Stretch.Fill;
            img.Width = 60;
            img.Height = 40;

            img.MouseDown += Img_MouseDown;

            AddDevice form = new AddDevice();

            if (form.ShowDialog() == true)
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(DeviceType.GetIcon(form.Result.TypeOfDevice), UriKind.Relative);
                bitmap.EndInit();
                img.Source = bitmap;
                Label label = new Label();
                label.Content = form.Result.Name;
                form.Result.Label = label;
                _deviceList.Add(img, form.Result);
                canvas.Children.Add(img);
                Canvas.SetLeft(label, p.X);
                Canvas.SetTop(label, p.Y + 35);
                canvas.Children.Add(label);
            }
            else
            {
                Debug.WriteLine("Přidání zařízení zrušeno!");
            }
        }

        /// <summary>
        /// Deaktivace přesunu objektu Img
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Img_MouseUp(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Middle:
                    drag = false;
                    break;
            }
        }

        /// <summary>
        /// Stisknutí tlačítka na myši
        /// Pouze pro objekt IMG
        /// Edit/delete/Spojení mezi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Img_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Debug.WriteLine(e.ChangedButton);
            if (!_line)
            {
                switch (e.ChangedButton)
                {
                    case MouseButton.Left:
                        Device device = _deviceList[(Image)sender];
                        DeviceManager dm = new DeviceManager(device);
                        dm.EditDevice(device);
                        dm.ShowDialog();
                        //Úprava labelu zařízení
                        int position = canvas.Children.IndexOf((Label)device.Label);
                        device = dm.EditedDevice;
                        ((Label)device.Label).Content = device.Name;
                        canvas.Children[position] = (Label)device.Label;
                        BitmapImage bitmap = new BitmapImage();
                        bitmap.BeginInit();
                        bitmap.UriSource = new Uri(DeviceType.GetIcon(device.TypeOfDevice), UriKind.Relative);
                        bitmap.EndInit();
                        ((Image) sender).Source = bitmap;
                        break;
                    case MouseButton.Middle:
                        drag = true;
                        _selectedImage = (Image)sender;
                        break;
                    case MouseButton.Right:
                        if (MessageBox.Show("Opravdu chcete zvolené zařízení smazat?", "Smazat zařízení", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            Device dev = _deviceList[(Image)sender];
                            canvas.Children.Remove((Label)dev.Label);
                            _deviceList.Remove((Image)sender);
                            canvas.Children.Remove((Image)sender);
                            foreach (Connection connection in dev.Connections)
                            {
                                canvas.Children.Remove((Line)connection.Line);
                                Device target = _deviceList[(Image)connection.Target];
                                List<object> collection = new List<object>(target.Connections);
                                foreach (Connection targetConnection in collection)
                                {
                                    if (targetConnection.Target == (Image)sender)
                                    {
                                        target.Connections.Remove(targetConnection);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            else if (e.ChangedButton == MouseButton.Left)
            {
                Debug.WriteLine("Aktivace CTRL");
                if (_selectedPoint == null)
                {
                    Debug.WriteLine("prvni");
                    _selectedPoint = (Image)sender;
                }
                else if (_selectedPoint != (Image)sender)
                {
                    Debug.WriteLine("druhy");
                    double y1 = (double)_selectedPoint.GetValue(Canvas.TopProperty) + (_selectedPoint.Height / 2);
                    double x1 = (double)_selectedPoint.GetValue(Canvas.LeftProperty) + (_selectedPoint.Width / 2);
                    double y2 = (double)((Image)sender).GetValue(Canvas.TopProperty) + (((Image)sender).Height / 2);
                    double x2 = (double)((Image)sender).GetValue(Canvas.LeftProperty) + (((Image)sender).Width / 2);
                    Line line = CreateLine(x1, y1, x2, y2);
                    _deviceList[_selectedPoint].Connections.Add(new Connection(true, line, (Image)sender));
                    _deviceList[(Image)sender].Connections.Add(new Connection(false, line, _selectedPoint));
                    canvas.Children.Insert(0, line);
                    _selectedPoint = null;
                }
            }
        }

        /// <summary>
        /// Obslužná metoda pro spuštění TFTP serveru
        /// </summary>
        private void menuStartTftp_Click(object sender, RoutedEventArgs e)
        {
            if (TFTPService.IsConnected)
            {
                TFTPService.StopServer();
            }
            else
            {
                TFTPService.Report += TftpStatusHandler;
                TFTPService.StartServer();
            }
            ChechStateOfTFTP();
        }

        /// <summary>
        /// Deaktivace kreslení spojení
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                _line = false;
            }
        }

        /// <summary>
        /// Aktivace kreslení spojení
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                _line = true;
            }
        }

        /// <summary>
        /// Pomocná metoda pro obsluhu textu Menu -> TFTP tlačítko
        /// </summary>
        private void ChechStateOfTFTP()
        {
            if (!TFTPService.IsConnected)
            {
                menuStartTftp.Header = "_Spustit TFTP server";
            }
            else
            {
                menuStartTftp.Header = "_Vypnout TFTP server";
            }
        }

        /// <summary>
        /// Uzavření aplikace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCloseApp_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Pomocná metoda pro vytvoření objektu Line
        /// </summary>
        private Line CreateLine(double x, double y, double x2, double y2)
        {
            Line line = new Line();
            line.X1 = x;
            line.Y1 = y;
            line.X2 = x2;
            line.Y2 = y2;
            line.Stroke = System.Windows.Media.Brushes.Black;
            line.StrokeThickness = 2;
            return line;
        }

        /// <summary>
        /// Přesun spojení v canvasu
        /// </summary>
        /// <param name="line"></param>
        /// <param name="first">Od</param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns>Nová linka</returns>
        private Line MoveLine(Line line, bool first, double x, double y)
        {
            canvas.Children.Remove(line);
            if (first)
            {
                line.X1 = x;
                line.Y1 = y;
            }
            else
            {
                line.X2 = x;
                line.Y2 = y;
            }
            canvas.Children.Insert(0, line);
            return line;
        }


        /// <summary>
        /// Handler TFTP
        /// vyplňuje údálosti z TFTP do Main statusbaru.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TftpStatusHandler(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                lblStatus.Text = sender.ToString();
            });
        }
    }
}

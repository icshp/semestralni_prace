﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using cisco.Core.Services;
using cisco.Core.Models;

namespace cisco.Views
{
    /// <summary>
    /// Logika pro obsluhu okna DeviceManager.xaml
    /// </summary>
    public partial class DeviceManager : Window
    {
        public Device EditedDevice { get; private set; }


        public DeviceManager(Device device)
        {
            InitializeComponent();
            EditedDevice = device;
            PrepareView();
        }

        /// <summary>
        /// Připrava scény k zobrazení. Inicializace zdrojů dat.
        /// </summary>
        private void PrepareView() {
            cbCOM.ItemsSource = null;
            cbCOM.ItemsSource = SerialLineService.GetSerialLines();
            cbDeviceType.ItemsSource = null;
            cbDeviceType.ItemsSource = DeviceType.GetTypes();
            EditBox.Visibility = Visibility.Hidden;
            if(EditedDevice.Credentials!=null)
                if (EditedDevice.Credentials.Exists(x => x.Type == CredentialType.SSH))
                {
                    Credential cred = EditedDevice.Credentials.Find(x => x.Type == CredentialType.SSH);
                    tbSshUsername.Text = cred.Username;
                    tbSshPassword.Text = cred.Password;
                }
        }

        /// <summary>
        /// Přepnutí fromuláře do režimu "Upravit zařízení" a načtení dat z editovaného zařízení.
        /// </summary>
        public void EditDevice(Device device) {
            lblOperationType.Content = "Upravit zařízení";
            EditBox.Visibility = Visibility.Visible;
            EditedDevice = device;
            tbDeviceName.Text = device.Name;
            tbDeviceDescription.Text = device.Description;
            cbDeviceType.SelectedItem = device.TypeOfDevice;
            tbDeviceIP.Text = Convert.ToString(device.IP);
            cbCOM.SelectedItem = device.SerialPort;
            if (device.SerialPort != null)
            {
                cbDeviceSerial.IsChecked = true;
            }
            cbDeviceSSH.IsChecked = device.SSHEnabled;
            cbDeviceTelnet.IsChecked = device.TelnetEnabled;
        }


        /// <summary>
        /// Uložení zařízení nového/editovaného
        /// </summary>
        public void SaveDevice()
        {
            if (tbDeviceName.Text != "" && tbDeviceName.Text.Length < 10)
            {
                EditedDevice.Name = tbDeviceName.Text;
                EditedDevice.Description = tbDeviceDescription.Text;
                EditedDevice.TypeOfDevice = (DeviceTypeEnum)cbDeviceType.SelectedItem;
                EditedDevice.IP = tbDeviceIP.Text;
                if (cbDeviceSerial.IsChecked == true && cbCOM.SelectedItem != null)
                {
                    EditedDevice.SerialPort = (string)cbCOM.SelectedItem;
                }
                else
                {
                    EditedDevice.SerialPort = null;
                }
                EditedDevice.SSHEnabled = (bool)cbDeviceSSH.IsChecked;
                Credential credential = new Credential();
                credential.Username = tbSshUsername.Text;
                credential.Password = tbSshPassword.Text;
                credential.Type = CredentialType.SSH;
                if(EditedDevice.Credentials != null)
                    if(EditedDevice.Credentials.Exists(x => x.Type == CredentialType.SSH))
                    {
                        EditedDevice.Credentials.Remove(EditedDevice.Credentials.Find(x => x.Type == CredentialType.SSH));
                    }
                EditedDevice.Credentials.Add(credential);
                EditedDevice.TelnetEnabled = (bool)cbDeviceTelnet.IsChecked;
                EditedDevice = EditedDevice;
                this.Close();
            }
            else {
                MessageBox.Show("Není vyplněno jméno zařízení, nebo je název příliš dlouhý! Doplňte prosím.", "Nekompletní vstup", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Metoda pro připojení pomocí seriové linky
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeviceConnectToSerial_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DeviceConfigurator dc = new DeviceConfigurator(EditedDevice, ConnectionType.SERIAL, cbCOM.Text);
                dc.ShowDialog();
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Metoda pro připojení pomocí protokolu Telnet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeviceConnectToTelnet_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DeviceConfigurator dc = new DeviceConfigurator(EditedDevice, ConnectionType.TELNET, tbDeviceIP.Text);
                dc.ShowDialog();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Připojení pomocí protokolu SSH
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeviceConnectToSSH_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (tbSshUsername.Text.Length > 0 && tbSshPassword.Text.Length > 0)
                {
                    DeviceConfigurator dc = new DeviceConfigurator(EditedDevice, ConnectionType.SSH, tbDeviceIP.Text, tbSshUsername.Text, tbSshPassword.Text);
                    dc.ShowDialog();
                } else
                {
                    MessageBox.Show("Nejsou vyplněny přihlašovací údaje!");
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Obslužná metoda pro uložení zařízení a následné obnovení zobrazení.
        /// </summary>
        private void btnDeviceSave_Click(object sender, RoutedEventArgs e)
        {
            SaveDevice();
        }

        /// <summary>
        /// Obslužná metoda pro uzavření formuláře "DeviceManager.xaml"
        /// </summary>
        private void MenuClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

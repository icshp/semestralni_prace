﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using cisco.Core.Helper;
using cisco.Core.Models;
using cisco.Core.Services;
using cisco.Helper;
using cisco.Models;
using cisco.Views.Pages.Commands;
using cisco.Views.Pages.DeviceConfigurator.Router;
using cisco.Views.Pages.DeviceConfigurator.Shared;

namespace cisco.Views
{

    public partial class DeviceConfigurator : Window
    {
        private Device _device;
        private ConnectionType _connectionType;
        private string _address, _username, _password;
        private object _connection;
        private delegate void Connection();

        /// <summary>
        /// Konstruktor pro Serial line a telnet
        /// </summary>
        /// <param name="device"></param>
        /// <param name="type"></param>
        /// <param name="address"></param>
        public DeviceConfigurator(Device device, ConnectionType type, string address)
        {
            _device = device;
            _connectionType = type;
            _address = address;
            Initialize();
        }

        /// <summary>
        /// Konstruktor pro SSH
        /// </summary>
        /// <param name="device"></param>
        /// <param name="type"></param>
        /// <param name="address"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public DeviceConfigurator(Device device, ConnectionType type, string address, string username, string password) {
            _device = device;
            _connectionType = type;
            _address = address;
            _username = username;
            _password = password;
            Initialize();
        }

        /// <summary>
        /// Inicializace
        /// </summary>
        private void Initialize()
        {
            InitializeComponent();
            LvPages.ItemsSource = CreateConfigurationList();
            LvCommands.ItemsSource = CommandPageHelper.commands;
            switch (_connectionType)
            {
                case ConnectionType.SERIAL:
                    Connect(ConnectSerial);
                        break;
                case ConnectionType.TELNET:
                    Connect(ConnectTelnet);
                    break;
                case ConnectionType.SSH:
                    Connect(ConnectSsh);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Připojení s využití delegátu
        /// </summary>
        /// <param name="connection"></param>
        private void Connect(Connection connection)
        {
            try
            {
                connection();
            }
            catch (Exception)
            {
                MessageBox.Show("Připojení k cíli se nezdařilo!", "Chyba spojení", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
        }

        /// <summary>
        /// Při zobrazení konfigurace, načte konfigurační stránky
        /// (Router)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvPages_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<string, string> selectedItem = (KeyValuePair<string, string>)LvPages.SelectedItem;

            if (_device.TypeOfDevice == DeviceTypeEnum.ROUTER)
            {
                switch (selectedItem.Value)
                {
                    case "RIP":
                        RIPPage rp = new RIPPage();
                        FPage.Content = rp;
                        break;
                    case "SS":
                        StaticRoutingPage ss = new StaticRoutingPage();
                        FPage.Content = ss;
                        break;
                    default:
                        SharedConfiguration(selectedItem.Value);
                        break;
                }
            }
            else if (_device.TypeOfDevice == DeviceTypeEnum.SWITCH)
            {
                SharedConfiguration(selectedItem.Value);
            }
        }

        /// <summary>
        /// Nastavení sdílené konfigurace 
        /// </summary>
        /// <param name="typeCode"></param>
        private void SharedConfiguration(string typeCode)
        {
            switch (typeCode)
            {
                case "ZN":
                    BasicConfigurationPage zn = new BasicConfigurationPage();
                    FPage.Content = zn;
                    break;
                case "NV":
                    VLANPage nv = new VLANPage();
                    FPage.Content = nv;
                    break;
            }
        }

        /// <summary>
        /// Vytvoří konfigurační stránku s požadovanými itemy.
        /// </summary>
        /// <returns></returns>
        private List<KeyValuePair<string, string>> CreateConfigurationList()
        {
            List<KeyValuePair<string, string>> configOptionsList = new List<KeyValuePair<string, string>>();
            configOptionsList.Add(new KeyValuePair<string, string>("Základní nastavení", "ZN"));
            configOptionsList.Add(new KeyValuePair<string, string>("Nastavení VLAN", "NV"));
            switch (_device.TypeOfDevice)
            {
                case DeviceTypeEnum.ROUTER:
                    configOptionsList.Add(new KeyValuePair<string, string>("Směrování RIP", "RIP"));
                    configOptionsList.Add(new KeyValuePair<string, string>("Statické směrování", "SS"));
                    //GENERIC
                    configOptionsList.Add(new KeyValuePair<string, string>("INTERFACE", "IR"));
                    break;
                case DeviceTypeEnum.SWITCH:
                    //GENERIC
                    configOptionsList.Add(new KeyValuePair<string, string>("INTERFACE", "IS"));
                    break;
                default:
                    throw new Exception("Nepodporované zařízení!");
            }
            return configOptionsList;
        }

        /// <summary>
        /// Při otevření stránky "Commands" se provede načtení dostupných commands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LvCommands_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CommandPage cp = new CommandPage((Command)LvCommands.SelectedItem);
            FCommandsPage.Content = cp;
        }

        /// <summary>
        /// Eventhandler
        /// Zobrazuje data, která jsou získána z připojení
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ConsoleDataHandler(object sender, EventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                string data = sender.ToString().Trim();
                tbConsoleView.Text += "\n"  + data;
            });
        }

        /// <summary>
        /// Odeslání příkazů
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConsoleSend_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (_connectionType)
                {
                    case ConnectionType.SERIAL:
                        ((SerialLineService)_connection).WriteLine(tbConsolePrompt.Text);
                        break;
                    case ConnectionType.TELNET:
                        ((TelnetService)_connection).WriteLine(tbConsolePrompt.Text);
                        break;
                    case ConnectionType.SSH:
                        ((SSHService)_connection).WriteLine(tbConsolePrompt.Text);
                        break;
                    default:
                        break;
                }
                tbConsolePrompt.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Chyba komunikace!" + ex.ToString());
            }
        }

        /// <summary>
        /// Metoda pro scrollování na konec (CLI)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbConsoleView_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbConsoleView.ScrollToEnd();
        }

        /// <summary>
        /// Uzavření připojení při zavření okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closed(object sender, System.ComponentModel.CancelEventArgs e)
        {
            switch (_connectionType)
            {
                case ConnectionType.SERIAL:
                    ((SerialLineService)_connection).Disconnect();
                    break;
                case ConnectionType.TELNET:
                    ((TelnetService)_connection).Disconnect();
                    break;
                case ConnectionType.SSH:
                    ((SSHService)_connection).Disconnect();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Připojení k seriové lince
        /// </summary>
        private void ConnectSerial()
        {
            _connection = new SerialLineService();
            ((SerialLineService)_connection).Connect(_address);
            ((SerialLineService)_connection).DataReceived += ConsoleDataHandler;
            lblConsoleInfo.Content = "Připojeno pomocí seriové linky na portu: " + _address;
        }

        /// <summary>
        /// Připojení pomocí telnetu
        /// </summary>
        private void ConnectTelnet()
        {
            _connection = new TelnetService();
            ((TelnetService)_connection).Connect(_address);
            ((TelnetService)_connection).DataReceived += ConsoleDataHandler;
            lblConsoleInfo.Content = "Připojeno pomocí protokolu telnet na adresu: " + _address;
        }

        /// <summary>
        /// Připojení pomocí Ssh
        /// </summary>
        private void ConnectSsh()
        {
            _connection = new SSHService();
            ((SSHService)_connection).Connect(_address, _username, _password);
            ((SSHService)_connection).DataReceived += ConsoleDataHandler;
            lblConsoleInfo.Content = "Připojeno pomocí protokolu SSH na adresu: " + _address;
        }

        /// <summary>
        /// Odeslání příkazu při stisku "enter"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbConsolePrompt_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Return)
            {
                btnConsoleSend_Click(sender, null);
            }
        }
    }
}
;
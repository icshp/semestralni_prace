﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cisco.Models;

using System.Text.Json;
using System.Text.Json.Serialization;
using System.IO;
using System.Text.Encodings.Web;
using Newtonsoft.Json;

namespace cisco.Helper
{

    public static class CommandPageHelper
    {
        public static List<Command> commands;

        static CommandPageHelper()
        {
            string jsonString = File.ReadAllText("Files/commands.json");
            commands = JsonConvert.DeserializeObject<List<Command>>(jsonString);
        }

    }
}
